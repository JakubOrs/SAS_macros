/* przykladowe typy: */

%* a            cytowane w "";
%* 1a           cytowane w'';
%* c            oddzielone przecinkami;


%macro quote_string_list(string,typ=1a);

	%if "&string." ne "" or %length(&string.) gt 0 %then
	%do;
		%local OutputString;

		%let OutputString = %sysfunc(catq("&typ.",%sysfunc(translate(&string,%str(,),%str( )))));

		&OutputString.
	%end;

%mend;